# Namibia Census 2023

## Links

* [Deployment Instructions](https://gitlab.com/NSADPC2020/NSADataProcessing/-/blob/master/DEPLOYMENT.md)

## Current Versions

* CSPro 7.7.3 02 August 2022
* CSEntry 7.7.3 02 August 2022
* CSWeb 7.7.0 12 February 2022

## Maps

### Icons

* View mode
  * **Red exclamation** (not started): No household in structure have been enumerated
  * **Orange hourglass** (in progress): At least one household is not started and another is either complete or partial
  * **Green checkmark** (complete): All households within structure are complete

### Structures on the map

Each structure is displayed on the map with a single map marker. Dwellings and households in the structure are managed by opening the structure menu. All structure, dwelling, and household data must be added, modified, deleted through the listing. A single GPS reading is taken for the entire structure before adding the structure to the listing. The map marker's description will initially come from the 1st household's head of household. Once enumeration starts the description will come from the 1st households head of household.

### Adding structures

From the menu select *Listing interview*. Select the form type, add structure, take GPS reading, and the listing application will be launched. Complete structure by completing listing questionnaire.

### Deleting structures, dwellings, and households

From the menu select *Delete interviews*. Household, institution, and special population interviews will have to be deleted before the underlying listing can be deleted. This is a safety precaution, so time intensive work is not accidently deleted.

### Restoring structures, dwellings, and households

Interviews cannot be restored after deletion.

## Multiple interviewers in an EA

The variable A_INTERVIEWER_CODE in the assignments guarantees all enumerations are unique. However, all interviewers assigned to EA will see all structures. It is the responsibility of the supervisor to make sure the interviewers are not enumerating the same structures.

## Working with Assignments

### Interviewer

Interviewers will only see work (structures, dwellings, and households) for their current assignment. They'll need to switch assignments to work in another EA.

### Supervisor

* When the supervisor logs in the selected assignment determines the assignment that will be added, deleted, or restored
* Supervisors will see all interviewers assigned to them regardless of the supervisor's current assignment

Assume that Sue is a supervisor and has two assignments:

* Assignment #1 = region 1, constituency 1, EA 1
* Assignment #2 = region 1, constituency 1, EA 2

and that Ian is an interviewer currently with a single assignment:

* Assignment #1 = region 1, constituency 1, EA 1

The Sue can assign Ian assignment #2 by doing the following:

* Sue logs into assignment #2
* Sue selects Ian
* Sue selects assignment #2
* Sue confirms assignment of assignment #2 to Ian

The process is similar for deleting an assignment.

### Reassignment workflow

A request was made to reassign households during the April 2023 mission. The scenario was the following. There are two enumerators in an EA. Enumerator #1 has listed the entire EA. However, the supervisor would like to add enumerator #2 to the EA.

* Enumerator #1 and enumerator #2 meet
* Enumerator #2 keys the listing interviews (which are short) from enumerator #1
* Enumerator # deletes their listing interviews
* Enumerator #2 begins interviews

### Headquarters

Supervisors do not have the ability to assign themselves new interviewers or assignments. HQ must add the interviewer or assignment in this case.

## Lock/Unlock interviews

By default interviewers are only able to list households, institutions, special populations. At the start of the census they will not be able to conduct census interviews. However, the supervisor has the ability to lock and unlock this functionality for the interviewer. To do this the supervisor will need to:

* Select "Lock/Unlock interviews"
* Confirm that they want to unlock all interviews
* Complete a Bluetooth synchronization with each interviewer they wish to unlock census interviews for

## Progress Report

* Currently there is a single progress report to track the progress of household interviews. The columns Complete Households, Partial Households, and Not Started Households represent the total number of households per interview with that status.
* To understand the statuses, know that a household interview cannot begin until the household has been listed. Also, the household interview consists of three questionnaires. The roster, individual, and household.
  * Not Started Households - Only the listing has been done for this household.
  * Partial Households - The listing has been completed and the household interview has begun. However, at least one of the following household interview questionnaires has not been completed (roster, individual, or household).
  * Completed Households - The listing has been completed. Also, all household interview questionnaires have been completed (roster, individual, or household).

## Dictionaries

### ASSIGNMENTS_DICT

The ASSIGNMENTS_DICT is created at headquarters. It can only be modified by headquarter's staff and downloaded via synchronization.

### OVERRIDE_ASSIGNMENTS_DICT

The OVERRIDE_ASSIGNMENTS_DICT allows supervisors to override what is in ASSIGNMENTS_DICT without writing to it. If the supervisor assigns an interviewer a new case the OVERRIDE_ASSIGNMENTS_DICT will contain this assignment and a status of "A" (assigned). Otherwise, if the supervisor deletes an assignment for an interviewer the OVERRIDE_ASSIGNMENTS_DICT will contain the deleted assignment with a status of "D" (deleted). When creating a dynamic value set of the interviewer's assignments the application developer will need to first loop through the ASSIGNMENTS_DICT. For each assignment assigned to the interviewer check in the OVERRIDE_ASSIGNMENTS_DICT if the status is "D." If it is than the assignment can be ignored, otherwise include it in the value set. Next, loop through the OVERRIDE_ASSIGNMENTS_DICT. For each assignment assigned to the interviewer check if the status is "A." If it is than add it to the value set, otherwise ignore it.

### WS_DICT

The working storage is used to generate templated reports.
