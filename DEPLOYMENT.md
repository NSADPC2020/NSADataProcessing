# Deployment Instructions

Deploying the project is a critical part of the census. You must understand and test this process. A mistake at this point could have serious consequences for the success of the census.

## Verify server URLs

* */Upload-Dictionaries/Upload-Dictionaries.csds*
* */Shared Logic/Server.apc*
  * Used by *Utility* batch application
  * Used by *Menu* CAPI application

## Initial Deploy

**WARNING** The initial deploy is only done once for your census. Re-deploying the project without deleting lookup files
from the web server and mobile devices will introduce duplicates cases in lookup files for all users that synchronize.

### Initial Deploy: Step 1 - Manual

* CSWeb - Delete *Deploy-Namibia-Census* folder using files dashboard
* CSWeb - Delete *Deploy-Namibia-Census-Timestamp.zip* folder using files dashboard
  * Initial deploy is only done once, so there shouldn't be other versions
* CSWeb - Delete each dictionary in the data dashboard (this will delete the data too)
* **ALL** Mobile devices - delete "*Deploy-Namibia-Census*" folder
(found in main storage > Android > data > gov.census.cspro.csentry > files > csentry )

### Initial Deploy: Step 2 - pffRunner

* Open *Initial-Build.pffRunner* and manually run each group
* Verify each group completes successfully with no errors before running the next
* Prepare Group
  * Deletes *Data* folder on the build machine
  * Deletes *Deploy-Namibia-Census* folder on the build machine
* Convert References Group
  * Converts each reference file *XL2CS* with the *Modify, add cases*
  * This includes the following lookup files Assignments.csdb, Geocodes.csdb, and Staff.csdb
* Upload Lookup Data Group
  * *Deploy-Dictionaries.pff* - Uploads all dictionaries used in synchronization to CSWeb
    * (collected data) listing, roster, individual, household, form b, form c, override dictionary
    * (lookup data) assignment, geocodes, staff
    * Also uploads the partial project as an app package, but it is not meant to be used
  * *Upload-Lookup-Data.pff* - Uploads lookup data (assignments, geocodes, and staff) to CSWeb
* Publish and Copy Files Group
  * *Device-Deploy.pff* - Publishes PENs and copies files to *Deploy-Namibia-Census* folder
  * The TPK files are not included, because they will be too large
  * You must deploy the TPK files separately
* Zip and Copy to Web Server Group
  * Zip *Deploy-Namibia-Census* as *Deploy-Namibia-Census-timestamp.zip"
  * Uploads zip to web server (*./csweb/files/Deploy-Namibia-Census-timestamp.zip*)

### Initial Deploy: Step 3 - Manual

* On web server unzip *Deploy-Namibia-Census-timestamp.zip* to a new folder named *Deploy-Namibia-Census*
  * Location on web server of zip will be *./csweb/files/Deploy-Namibia-Census-timestamp.zip*
  * The expected path on web server is *./csweb/files/Deploy-Namibia-Census/*
  * Note - TPKs will not be included, because they are too large to be downloaded
  * Note - Leave the zip on the server as a backup
* Delete the uploaded package "Ignore Updated Package" from the Apps page on CSWeb
* On the build machine copy the "*Tile Packages*" folder with the correct TPKs to *Deploy-Namibia-Census*
* Copy the *Deploy-Namibia-Census* folder from the build machine to the csentry folder on each device

## Update Lookups

**WARNING** Updating the lookups is meant to add / modify staff and assignments. You can also update the geocodes.
If an error is made duplicates will be pushed to all supervisors and interviewers and will cause confusion. For this reason,
I recommend only doing this if absolutely necessary. For example, modifying an EA in an existing assignment will make the EA
inaccessible to the interviewer. Instead of modify the EA you might simply add another assignment for the interviewer. However,
if the interviewer has already started enumerating then their work may be split between two EAs. The best thing may just be
to do nothing and document the issue. Then the cases with the incorrect EA can be imputed to the correct EA. Prefer workarounds
in the field to pushing multiple updates.

### Update Lookups: Step 1 - Manual

* Unzip previous set of backed up lookup files (whether from the initial deploy or an update of the lookups)
  * Copy *Assignments.csdb*, *Geocodes.csdb*, *Listing.csdb*, and *Staff.csdb* to *./namibia-census-2021/Data/*" on the build machine replacing the previous versions
* Add or modify rows the reference files in *./References/*
  * Assignments.xlsx, Geocodes.xlsx, Listing.xlsx, and Staff.xlsx
  * Extents.xlsx is currently ignored (correct coordinates have never been provided for the EAs)
* Deletion of rows will be ignored

### Update Lookups: Step 2 - Build lookups

* Open *Update-Lookups.pffRunner* and manually run each group
* Verify each group completes successfully with no errors before running the next
* Verify Lookup Group
  * Downloads lookup files from server
  * Then verify there are no duplicated cases (UUIDs) in the local CSDB files
* Prepare Group
  * Deletes *Deploy-Namibia-Census* folder on the build machine
* References Group
  * Converts each reference file *XL2CS* with the *Modify, add cases*
  * This includes the following lookup files Assignments.csdb, Geocodes.csdb, and Staff.csdb
  * The current lookup files must exist in ./Data/. **If not, duplicates will be added.**
* Upload Lookup Data Group
  * *Upload-Lookup-Data.pff* - Uploads lookup data (assignments, geocodes, and staff) to CSWeb
  * Note - The dictionaries are not updated (dangerous)
* Copy Lookups Group
  * *Device-Deploy.pff* - Publishes PENs and copies files to *Deploy-Namibia-Census* folder
  * Note - Calls *Device-Deploy.pff*, but only to copy lookups to destination for backup
* Zip and Copy to Web Server
  * Zip *Deploy-Namibia-Census* as *Deploy-Namibia-Census-timestamp.zip"
  * Upload to web server (*./csweb/files/Deploy-Namibia-Lookup.zip*)

## Explanation of duplicates in lookup files

All new lookup data should be added to the system through the Excel reference files. If the references are built without the setting *Modify, add cases* the CSDB files will be deleted and rebuilt. This results in new UUIDs being associated with each case. This will cause duplicates in the lookup files when synchronization occurs. Manually deleting the lookup CSDB files have the same negative effect.

Further, consider what could happen if the tablets and server were set up in advance of the census. Then for whatever reason, someone ran this script from another computer. The CSDB files would be rebuilt with new UUIDs and uploaded to the server. The tablets would have CSDB files with different UUIDs and when a synchronization occurs in the field all lookup files would have duplicates.

The *Upload Lookup Data* group is run before the *Publish and Copy Files* group so lookup files have a sync history that includes an upload to the server. This will allow the first synchronization in the field avoid downloadind all lookup data.

Note that the TPK files are not deployed as part of this process. They are large and you'll need to develop a plan to load only a portion of the country's TPKs on each tablet.